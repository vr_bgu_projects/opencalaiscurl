﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCalaisCurl
{
    public class OpenCalaisResult
    {
        public string image_id { get; set; }
        public string image_desc { get; set; }
        public string result { get; set; }
        public string result_date { get; set; }

        public OpenCalaisResult(string id, string text, string res, string res_date)
        {
            image_id = id;
            image_desc = text;
            result = res;
            result_date = res_date;
        }
    }
}
