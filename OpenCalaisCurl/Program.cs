﻿using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace OpenCalaisCurl
{
    class Program
    {
        public object Util { get; private set; }

        static void Main(string[] args)
        {
            string messageToUser = "first arg is api key, second arg is file with [id,iamge url] in new line";
            Console.WriteLine(messageToUser);

            if (args.Length != 2)
            {
                Console.WriteLine(messageToUser);
                Environment.Exit(0);
            }
            var lines = File.ReadAllLines(args[1]);

            List<OpenCalaisResult> resultsOpenCalais = new List<OpenCalaisResult>();

            System.IO.TextWriter writeFileOpenCalais = new StreamWriter("OpenCalaisResults.csv");
            var csvOpenCalais = new CsvWriter(writeFileOpenCalais);


            foreach (var line in lines)
            {
                try
                {
                    string id = line.Split(',')[0];
                    string image_desc = line.Split(',')[1];
                    Console.WriteLine("Image Id: " + id + ", Image desc: " + image_desc);

                        string analysisResult = OpenCalaisImageCurl(args[0], image_desc);
                        if (analysisResult != null)
                        {
                            string jsonString = analysisResult;
                            File.WriteAllText(id + "_openCalais.txt", jsonString);
                            OpenCalaisResult openCalaisResult = new OpenCalaisResult(id, image_desc, jsonString, DateTime.Now.ToShortDateString());
                            resultsOpenCalais.Add(openCalaisResult);
                            csvOpenCalais.WriteRecord(openCalaisResult);
                            writeFileOpenCalais.Flush();

                            int milliseconds = 2000;
                            Thread.Sleep(milliseconds);
                        }
                    

                
                    //break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    continue;
                }
            }

            

            //csvOpenCalais.WriteRecords(resultsOpenCalais);
            writeFileOpenCalais.Flush();
            writeFileOpenCalais.Close();
        }

        

        public static string OpenCalaisImageCurl(string APIKey, string imageDesc)
        {

            string openCalaisResultResponse = null;
            string result = "";
            try
            {
                string body = imageDesc.Replace("\"","'");

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.thomsonreuters.com/permid/calais\" -H \"Content-Type: text/plain\" -H \"x-ag-access-token: {0}\" -H \"outputFormat: application/json\" --data-ascii \"{1}\"",  APIKey, body);

                stopwatch.Start();
                result = RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                //Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds));

                openCalaisResultResponse = result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return openCalaisResultResponse;
        }


        


        public static string RunProcess(string processFileName, string arguments)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Console.WriteLine(line);
                }
                Console.WriteLine(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result.ToString();
        }



    }

 
}
